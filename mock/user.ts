import { Request, Response } from 'express'

const fetchUserInfo = (req: Request, res: Response) => {
  res.status(200).send({
    code: 0,
    data: {
      address: '浦东新区1号',
      avatar: 'https://pic.cnblogs.com/avatar/a100150.jpg',
      createTime: '2022-01-28 12:43:06',
      departmentIdList: [1],
      email: 'jikeytang@163.com',
      id: 1,
      mobile: '+86-18612345678',
      nickname: '管理员',
      phone: '+86-12345678',
      profile: '哥不是一个人在战斗',
      realName: 'admin',
      signature: '冗长的黑暗中，你是我唯一的光',
      status: 1,
      updateTime: '2022-01-28 17:49:52',
      username: 'admin',
    },
    msg: '操作成功',
  })
}

const fetchLogin = (req: Request, res: Response) => {
  const { password, username } = req.body
  if (password === '123456' && username === 'admin') {
    res.send({
      code: 0,
      data: {
        token: 'eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ',
        roles: 'Administration',
        resources:
          'menuTree,home,system:user:info,account,account:center,system:message:list,system:message:add,system:message:detail,system:message:batchDelete,system:message:update,system:message:delete,system:message:publish,system:message:status,system:message:timer,system:message:batchPublish,account:setting,system,system:department,system:department:tree,system:department:add,system:department:move,system:department:detail,system:department:update,system:department:delete,system:department:children,system:department:status,system:user,system:user:list,system:user:add,system:user:batchDelete,system:user:detail,system:user:update,system:user:delete,system:user:pwd:reset,system:user:pwd:update,system:user:roles,system:user:grant,system:user:status,system:role,system:role:tree,system:role:add,system:role:move,system:role:detail,system:role:update,system:role:delete,system:role:children,system:role:resources,system:role:grant,system:role:status,system:menu,system:menu:tree,system:menu:add,system:menu:move,system:menu:detail,system:menu:update,system:menu:delete,system:menu:children,system:menu:status,system:api,system:api:add,system:api:move,system:api:detail,system:api:update,system:api:delete,system:api:children,system:api:status,system:dictionary,system:dictionary:list,system:dictionary:add,system:dictionary:batchDelete,system:dictionary:detail,system:dictionary:update,system:dictionary:delete,system:dictionary:status',
      },
      msg: '操作成功',
    })
    return
  }
  if (password === '123456' && username === 'user') {
    res.send({
      token: 'eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ',
      roles: 'user,test1',
      resources:
        'menuTree,home,system:user:info,account,account:center,system:message:list,system:message:add,system:message:detail,system:message:batchDelete,system:message:update,system:message:delete,system:message:publish,system:message:status,system:message:timer,system:message:batchPublish,account:setting,system,system:department,system:department:tree,system:department:add,system:department:move,system:department:detail,system:department:update,system:department:delete,system:department:children,system:department:status,system:user,system:user:list,system:user:add,system:user:batchDelete,system:user:detail,system:user:update,system:user:delete,system:user:pwd:reset,system:user:pwd:update,system:user:roles,system:user:grant,system:user:status,system:role,system:role:tree,system:role:add,system:role:move,system:role:detail,system:role:update,system:role:delete,system:role:children,system:role:resources,system:role:grant,system:role:status,system:menu,system:menu:tree,system:menu:add,system:menu:move,system:menu:detail,system:menu:update,system:menu:delete,system:menu:children,system:menu:status,system:api,system:api:add,system:api:move,system:api:detail,system:api:update,system:api:delete,system:api:children,system:api:status,system:dictionary,system:dictionary:list,system:dictionary:add,system:dictionary:batchDelete,system:dictionary:detail,system:dictionary:update,system:dictionary:delete,system:dictionary:status',
    })
    return
  }
  // 为了保证错误请求也可以被登录页面单独处理，这里状态码要设为200.
  res.status(200).send({
    apierror: {
      status: 'error',
      message: '用户名密码错误。',
    },
  })
}

const fetchLogout = (req: Request, res: Response) => {
  res.status(200).send({
    code: 0,
    data: {},
    msg: '操作成功',
  })
}

export default {
  // 'GET  /api/user/captcha': fetchCaptcha,

  'POST /api/user/login': fetchLogin,

  'GET /api/user/info': fetchUserInfo,

  'POST /api/user/logout': fetchLogout,
}
