import { defineConfig } from 'umi'
import { theme, env } from './default-settings'
// import proxy from './proxy'

// const { NODE_ENV } = process.env
const base = '/react-umi-admin/'

export default defineConfig({
  define: env.dev,
  publicPath: base,
  nodeModulesTransform: {
    type: 'none',
  },
  mock: false,
  fastRefresh: {},
  hash: true,
  history: {
    type: 'hash',
  },
  title: false,
  antd: {
    config: {},
  },
  webpack5: {
    lazyCompilation: {},
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  theme,
  cssModulesTypescriptLoader: {},
  ignoreMomentLocale: true,
  // proxy: proxy.dev,
})
