import { Settings } from '@ant-design/pro-layout'

const settings: Settings = {
  navTheme: 'dark',
  // 拂晓蓝
  primaryColor: '#409EFF',
  layout: 'side',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: 'React UMI Admin',
  iconfontUrl: '',
}

export default settings

export const theme = {
  'primary-color': '#409EFF',
  'layout-header-background': '#000',
  'menu-bg': '#000',
  'menu-dark-bg': '#000',
  'menu-dark-submenu-bg': '#151515',
}

export const env = {
  dev: {
    'process.env.REACT_APP_BASE_URL': 'https://www.fastmock.site/mock/8a027582c169e035aa9cd404b3dbc06f/api',
    'process.env.REACT_APP_VERSION': '1.0.0',
  },
}
