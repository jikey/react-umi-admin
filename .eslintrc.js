module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['react', 'babel', 'react-hooks', '@typescript-eslint'],
  extends: [
    'eslint:all',
    'react-app', //  react帮配置好了一些语法，譬如箭头函数
    'airbnb',
  ],
  rules: {
    'react/react-in-jsx-scope': 'off',
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 'off',
    'import/no-unresolved': 0,
    'default-param-last': 0,
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx', 'tsx'] }], // 关闭airbnb对于jsx必须写在jsx文件中的设置
    'react/prop-types': 'off', // 关闭airbnb对于必须添加prop-types的校验
    'react/destructuring-assignment': [
      1,
      'always',
      {
        ignoreClassFields: false,
      },
    ],
    'react/jsx-one-expression-per-line': 'off', // 关闭要求一个表达式必须换行的要求，和Prettier冲突了
    'react/jsx-wrap-multilines': 0, // 关闭要求jsx属性中写jsx必须要加括号，和Prettier冲突了
    // 'comma-dangle': ['error', 'never'],
    'react/jsx-first-prop-new-line': [1, 'multiline-multiprop'],
    'react/prefer-stateless-function': [0, { ignorePureComponents: true }],
    'jsx-a11y/no-static-element-interactions': 'off', // 关闭非交互元素加事件必须加 role
    'jsx-a11y/click-events-have-key-events': 'off', // 关闭click事件要求有对应键盘事件
    'no-bitwise': 'off', // 不让用位操作符，不知道为啥，先关掉
    'react/jsx-indent': [2, 2],
    'react/jsx-no-undef': [2, { allowGlobals: true }],
    'jsx-control-statements/jsx-use-if-tag': 0,
    'react/no-array-index-key': 0,
    'react/jsx-props-no-spreading': 0,
    // 禁止使用 var
    'no-var': 'error',
    semi: ['error', 'never'],
    quotes: [2, 'single'],
    // @fixable 必须使用 === 或 !==，禁止使用 == 或 !=，与 null 比较时除外
    eqeqeq: [
      'warn',
      'always',
      {
        null: 'ignore',
      },
    ],
    // 'no-use-before-define': ['error', { functions: false }],
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    'import/prefer-default-export': 'off',
    'object-curly-newline': 0,
    'linebreak-style': ['off', 'windows'],
    'no-alert': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    '@typescript-eslint/no-unused-vars': [2, { args: 'none' }],
    'no-unused-expressions': 'off',
    '@typescript-eslint/no-unused-expressions': 2,
    // 'arrow-body-style': ['error', 'as-needed'],
    'arrow-body-style': [0, 'never'],
    // 'prettier/prettier': ['error', { parser: 'flow' }],
    'jsx-a11y/label-has-associated-control': 0,
    'jsx-a11y/label-has-for': 0,
    'react/require-default-props': 0,
    'react/default-props-match-prop-types': 0,
    'jsx-a11y/alt-text': 0,
    camelcase: 0,
    'react-hooks/exhaustive-deps': 'off',
    'global-require': 0,
    'import/no-dynamic-require': 0,
    'no-param-reassign': 0,
    'sort-imports': 'off',
    'import/order': 'off',
    'react/function-component-definition': 0,
    'react/no-unstable-nested-components': 'off',
    'consistent-return': 0,
    'max-len': 0,
    'react/jsx-no-useless-fragment': 0,
    'jsx-a11y/media-has-caption': 0,
  },
  globals: {
    API: 'readonly',
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      plugins: [
        '@typescript-eslint',
        // 'plugin:@typescript-eslint/recommended'
      ],
    },
  ],
}
