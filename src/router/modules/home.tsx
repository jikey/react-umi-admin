import { FileTextOutlined, DashboardOutlined, FileWordOutlined, MessageOutlined } from '@ant-design/icons'

export default {
  path: '/home',
  name: '首页',
  routes: [
    {
      path: '/home/dashboard',
      name: '公告板',
      icon: <DashboardOutlined />,
    },
    {
      path: '/home/doc',
      name: '文档',
      icon: <FileWordOutlined />,
    },
    {
      path: '/home/qa',
      name: '反馈页面',
      icon: <MessageOutlined />,
      routes: [
        {
          path: '/home/qa/success',
          name: '操作成功',
          icon: <FileTextOutlined />,
        },
        {
          path: '/home/qa/error',
          name: '操作失败',
          icon: <FileTextOutlined />,
        },
      ],
    },
  ],
}
