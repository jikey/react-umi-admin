export default {
  path: '/form',
  name: '表单',
  routes: [
    {
      menuName: '普通表单',
      name: '普通表单',
      path: '/form/index',
      routes: [
        {
          name: '基础表单',
          path: '/form/basic',
        },
        {
          name: '分步表单',
          path: '/form/step',
        },
        {
          name: '高级表单',
          path: '/form/high',
        },
      ],
    },
    {
      menuName: '表格',
      name: '表格',
      path: '/form/table',
      routes: [
        {
          name: '基础表格',
          path: '/form/table/edit',
        },
        // {
        //   name: '分步表单',
        //   path: '/form/step',
        // },
        // {
        //   name: '高级表单',
        //   path: '/form/high',
        // },
      ],
    },
  ],
}
