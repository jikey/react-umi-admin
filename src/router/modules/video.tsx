export default {
  path: '/video',
  name: '视频',
  routes: [
    {
      menuName: '普通视频',
      name: '普通视频',
      path: '/video/index',
      routes: [
        {
          name: '单视频',
          path: '/video/basic',
        },
        {
          name: '双视频',
          path: '/video/audio',
        },
      ],
    },
    {
      menuName: '普通音频',
      name: '普通音频',
      path: '/audio/index',
      routes: [
        {
          name: '基础音频',
          path: '/audio/basic',
        },
        {
          name: '多音频',
          path: '/audio/more',
        },
      ],
    },
  ],
}
