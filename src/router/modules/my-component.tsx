export default {
  path: '/my-component',
  name: '组件',
  routes: [
    {
      menuName: '组件',
      name: '组件',
      path: '/my-component/index',
      routes: [
        {
          name: '富文体',
          path: '/my-component/tinymce',
        },
        {
          name: 'Markdown',
          path: '/my-component/markdown',
        },
        {
          name: '返回顶部',
          path: '/my-component/back-to-top',
        },
      ],
    },
  ],
}
