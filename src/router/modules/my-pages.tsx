export default {
  path: '/my-pages',
  name: '页面',
  routes: [
    {
      menuName: '列表页',
      name: '列表页',
      path: '/my-pages/index',
      routes: [
        {
          name: '查询表格',
          path: '/my-pages/table',
        },
        {
          name: '标准表格',
          path: '/my-pages/standard',
        },
        {
          name: '卡片列表',
          path: '/my-pages/card-list',
        },
      ],
    },
  ],
}
