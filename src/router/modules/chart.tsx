export default {
  path: '/chart',
  name: '图表',
  routes: [
    {
      menuName: '图表',
      name: '图表',
      path: '/chart/index',
      routes: [
        {
          name: '折线图',
          path: '/chart/line',
        },
        {
          name: '柱状图',
          path: '/chart/bar',
        },
        {
          name: '饼图',
          path: '/chart/pie',
        },
        {
          name: '时间线图',
          path: '/chart/timeline',
        },
      ],
    },
  ],
}
