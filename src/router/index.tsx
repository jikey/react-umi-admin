import React from 'react'
import {
  FileTextOutlined,
  BulbOutlined,
  AppstoreOutlined,
  GlobalOutlined,
  AreaChartOutlined,
  Html5Outlined,
  HeartOutlined,
  EnvironmentOutlined,
  FormOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'
import Home from './modules/home'
import Chart from './modules/chart'
import MyComponent from './modules/my-component'
import Form from './modules/form'
import MyPages from './modules/my-pages'
import Video from './modules/video'

export const rootRoutes = [
  {
    path: '/home',
    name: '常用',
    icon: <GlobalOutlined />,
  },
  {
    path: '/my-component',
    name: '组件',
    icon: <AppstoreOutlined />,
  },
  {
    path: '/form',
    name: '表单',
    icon: <FormOutlined />,
  },
  {
    path: '/my-pages',
    name: '页面',
    icon: <FileTextOutlined />,
  },
  {
    path: '/chart',
    name: '图表',
    icon: <AreaChartOutlined />,
  },
  {
    path: '/canvas',
    name: 'Canvas',
    icon: <Html5Outlined />,
  },
  {
    path: '/svg',
    name: 'SVG',
    icon: <HeartOutlined />,
  },
  {
    path: '/map',
    name: '地图',
    icon: <EnvironmentOutlined />,
  },
  {
    path: '/video',
    name: '视频',
    icon: <VideoCameraOutlined />,
  },
  {
    path: '/other',
    name: '其它',
    icon: <BulbOutlined />,
  },
]

export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  Home,
  MyComponent,
  Form,
  Chart,
  MyPages,
  Video,
]
