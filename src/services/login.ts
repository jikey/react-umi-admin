import { request } from 'umi'
import { LoginParamsType } from '@/types/login'

/**
 * 用户登录
 * @param data
 */
export async function fetchLogin(data: LoginParamsType) {
  return request('/user/login', {
    method: 'POST',
    data,
  })
}

/**
 * 退出登录
 * @param data
 */
export async function fetchLogout() {
  return request('/user/logout', {
    method: 'POST',
  })
}
