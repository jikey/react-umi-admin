import React from 'react'
import { Space } from 'antd'
import { PageContainer } from '@ant-design/pro-layout'

const StandardPages: React.FC = () => {
  return (
    <PageContainer>
      <Space>My pages Index</Space>
    </PageContainer>
  )
}
export default StandardPages
