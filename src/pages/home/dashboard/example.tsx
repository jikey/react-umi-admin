import React from 'react'
import { Space } from 'antd'
import { PageContainer } from '@ant-design/pro-layout'
import QueueAnim from 'rc-queue-anim'

const DashboardExample: React.FC = () => {
  return (
    <PageContainer>
      <Space>Example</Space>
      <QueueAnim delay={300} className="queue-simple">
        <div key="a">Queue Demo</div>
        <div key="b">Queue Demo</div>
        <div key="c">Queue Demo</div>
        <div key="d">Queue Demo</div>
      </QueueAnim>
    </PageContainer>
  )
}
export default DashboardExample
