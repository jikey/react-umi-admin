import React from 'react'
import { Button, Descriptions, Result } from 'antd'
import { PageContainer, PageContainerProps } from '@ant-design/pro-layout'
import { useModel } from 'umi'

const Dashboard: React.FC = () => {
  const { StorgeGet } = useModel('storge')
  const { nickname, address, email, qq, createTime, signature } = StorgeGet({ path: 'info' })
  const content = (
    <Descriptions size="small" column={2}>
      <Descriptions.Item label="创建人">{nickname}</Descriptions.Item>
      <Descriptions.Item label="联系方式">{email}</Descriptions.Item>
      <Descriptions.Item label="QQ 群">{qq}</Descriptions.Item>
      <Descriptions.Item label="更新时间">{createTime}</Descriptions.Item>
      <Descriptions.Item label="地址">{address}</Descriptions.Item>
      <Descriptions.Item label="签名">{signature}</Descriptions.Item>
    </Descriptions>
  )
  const pageProps: PageContainerProps = {
    content,
    breadcrumbRender: false,
  }

  return (
    <PageContainer {...pageProps}>
      <div style={{ height: 'calc(100vh - 385px)' }}>
        <Result
          status="404"
          style={{
            height: '100%',
            background: '#fff',
          }}
          title="Hello World"
          subTitle="Sorry, you are not authorized to access this page."
          extra={<Button type="primary">Back Home</Button>}
        />
      </div>
    </PageContainer>
  )
}
export default Dashboard
