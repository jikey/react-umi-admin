import React from 'react'
import { Card, Space } from 'antd'
import { PageContainer } from '@ant-design/pro-layout'
import QueueAnim from 'rc-queue-anim'

const DashboardExample: React.FC = () => {
  const list = new Array(100).fill(1)
  return (
    <PageContainer>
      <Space>Doc</Space>
      <Space direction="vertical" size={24} style={{ width: '100%' }}>
        <Card>
          <QueueAnim delay={300} className="queue-simple">
            <div key="a">Queue Demo</div>
            <div key="b">Queue Demo</div>
            <div key="c">Queue Demo</div>
            <div key="d">Queue Demo</div>
          </QueueAnim>
        </Card>
        <Card>
          <h1>测试页长</h1>
          {list.map((item: any) => {
            return <div>{item}</div>
          })}
        </Card>
      </Space>
    </PageContainer>
  )
}
export default DashboardExample
