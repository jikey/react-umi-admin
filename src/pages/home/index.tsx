import { useEffect } from 'react'
import { history } from 'umi'

export default function IndexPage() {
  useEffect(() => {
    history.push('/home/dashboard')
  }, [])

  return (
    <div>
      <h1>Page index</h1>
    </div>
  )
}
