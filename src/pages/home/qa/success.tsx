import React from 'react'
import { Space } from 'antd'
import { PageContainer } from '@ant-design/pro-layout'

const DashboardExample: React.FC = () => {
  return (
    <PageContainer>
      <Space>Success</Space>
    </PageContainer>
  )
}
export default DashboardExample
