import { FC } from 'react'
import { Card } from 'antd'
import { Helmet } from 'umi'

const Blank: FC = () => {
  return (
    <>
      <Helmet>
        <title>空白页面的标题</title>
        <meta name="keywords" content="key-空白页面" />
      </Helmet>
      <div className="app-guide">
        <Card type="inner" title="开发文档">
          <p>开发文档正在紧张编写中...</p>
        </Card>
      </div>
    </>
  )
}

export default Blank
