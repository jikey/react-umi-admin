import { FC, useState, useEffect } from 'react'
import TimeLine from '@/components/Chart/TimeLine'

const TimeLinePage: FC = () => {
  const [loading, setLoading] = useState<boolean>(true)

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(false)
    }, 2000)

    return () => {
      clearTimeout(timer)
    }
  }, [])

  return (
    <div>
      <TimeLine loading={loading} />
    </div>
  )
}

export default TimeLinePage
