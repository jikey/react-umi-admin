import React from 'react'
import { Card } from 'antd'
import LineChart from '@/components/Chart/LineChart'

const StandardPages: React.FC = () => {
  const lineChartData = {
    loading: false,
    // 折线图模拟数据
    xData: ['2021/08/13', '2021/08/14', '2021/08/15', '2021/08/16', '2021/08/17', '2021/08/18'],
    seriesData: [22, 19, 88, 66, 5, 90],
  }

  return (
    <Card>
      <LineChart
        width={800}
        height={600}
        loading={lineChartData.loading}
        xData={lineChartData.xData}
        seriesData={lineChartData.seriesData}
      />
    </Card>
  )
}
export default StandardPages
