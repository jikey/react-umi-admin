import React, { useEffect } from 'react'
import { history } from 'umi'

const MyPages: React.FC = () => {
  useEffect(() => {
    history.push('/form/table/edit')
  }, [])

  return (
    <div>
      <h1>Page index</h1>
    </div>
  )
}
export default MyPages
