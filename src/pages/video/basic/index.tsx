import React from 'react'
import View from '@/components/View'
import MyVideo from '@/components/Video'

const VideoPages: React.FC = () => {
  const videoList = [
    { url: 'http://vfx.mtime.cn/Video/2019/03/19/mp4/190319222227698228.mp4', title: '玩具总动员' },
    { url: 'http://vfx.mtime.cn/Video/2019/03/18/mp4/190318214226685784.mp4', title: '银都机构' },
  ]

  return (
    <View title="视频组件调用">
      <MyVideo videoList={videoList} />
    </View>
  )
}
export default VideoPages
