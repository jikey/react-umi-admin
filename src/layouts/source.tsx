import { useLocation } from 'react-router-dom'
import { Gitee } from '@/common/constants'

const Source = () => {
  const { pathname } = useLocation()
  const url = `${Gitee}/blob/master/src/pages${pathname}/index.tsx`

  return (
    <div className="app-source">
      <a href={url} target="_blank" rel="noopener noreferrer">
        &lt; code /&gt;
      </a>
    </div>
  )
}

export default Source
