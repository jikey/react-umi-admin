import React, { FC } from 'react'
import { SettingOutlined } from '@ant-design/icons'
import { Tooltip } from 'antd'
import { useModel } from 'umi'

const Setting: FC = () => {
  const { settingDrawerCollapse, setSettingDrawerCollapse } = useModel('global')
  const onSetting = () => {
    setSettingDrawerCollapse(!settingDrawerCollapse)
  }
  return (
    <div className="app-setting" onClick={onSetting}>
      <Tooltip title="设置">
        <SettingOutlined />
      </Tooltip>
    </div>
  )
}

export default Setting
