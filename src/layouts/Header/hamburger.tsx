import React, { FC } from 'react'
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons'

export interface IProps {
  collapsed?: boolean
  setCollapsed: Function
}

const Hamburger: FC<IProps> = ({ collapsed, setCollapsed }) => {
  return (
    <div className="app-hamburger" onClick={() => setCollapsed(!collapsed)}>
      {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
    </div>
  )
}

export default Hamburger
