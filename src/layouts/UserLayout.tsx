import React, { FC } from 'react'
import { Helmet } from 'umi'
import { MenuDataItem, BasicLayoutProps as ProLayoutProps } from '@ant-design/pro-layout'
import { ProjectKeywords, ProjectName } from '@/common/constants'
import Footer from '@/components/footer'
import logo from '@/assets/logo.svg'
import styles from './less/UserLayout.less'

export interface UserLayoutProps {
  breadcrumbNameMap?: {
    [path: string]: MenuDataItem
  }
  location?: ProLayoutProps['location']
  route?: ProLayoutProps['route']
}

const UserLayout: FC<UserLayoutProps> = (props) => {
  const { children } = props

  return (
    <div className="app-user-layout">
      <Helmet>
        <title>Login | {ProjectName}</title>
        <meta name="keywords" content={ProjectKeywords} />
      </Helmet>
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
              <img alt="logo" className={styles.logo} src={logo} />
              <span className={styles.title}>{ProjectName}</span>
            </div>
            <div className={styles.desc}>{ProjectKeywords}</div>
          </div>
          {children}
        </div>
        <Footer />
      </div>
    </div>
  )
}

export default UserLayout
