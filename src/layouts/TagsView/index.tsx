import React from 'react'
import TagList from './components/TagList'

const TagsView = () => {
  return (
    <div className="app-tagsview">
      <TagList />
    </div>
  )
}

export default TagsView
