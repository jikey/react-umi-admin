import React, { FC } from 'react'
import { ProSettings, SettingDrawer } from '@ant-design/pro-layout'
import { useModel } from 'umi'

export interface SettingsProps {
  pathname: string
  settings: Partial<ProSettings> | undefined
  setSetting: any
}

const AppSettings: FC<SettingsProps> = (props) => {
  const { settingDrawerCollapse, setSettingDrawerCollapse } = useModel('global')
  const { pathname, settings, setSetting } = props
  const drawerProps = {
    hideHintAlert: true,
    hideCopyButton: true,
    disableUrlParams: true,
    collapse: settingDrawerCollapse,
    onCollapseChange: setSettingDrawerCollapse,
  }

  return (
    <div className="app-settings-drawer">
      <SettingDrawer
        pathname={pathname}
        enableDarkTheme
        getContainer={() => document.getElementById('app-basic-pro-layout')}
        settings={settings}
        onSettingChange={(changeSetting: Partial<ProSettings>) => {
          console.log('changeSetting', changeSetting)
          setSetting(changeSetting)
        }}
        {...drawerProps}
      />
    </div>
  )
}

export default AppSettings
