import { useState } from 'react'
import { db } from '@/utils/db'

export default () => {
  const [storge, setStorge] = useState()

  const reducers = {
    StorgeSet({ dbName = 'user', path, value, user = true }: any) {
      db.dbSet({ dbName, path, value, user })
    },
    StorgeGet({ dbName = 'user', path, defaultValue = {}, user = true }: any) {
      return db.dbGet({ dbName, path, defaultValue, user })
    },
  }

  return {
    storge,
    setStorge,
    ...reducers,
  }
}
