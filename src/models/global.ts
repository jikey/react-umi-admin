import { useState } from 'react'

export default () => {
  const [settingDrawerCollapse, setSettingDrawerCollapse] = useState(false)

  return {
    settingDrawerCollapse,
    setSettingDrawerCollapse,
  }
}
