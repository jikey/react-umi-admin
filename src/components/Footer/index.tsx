import React from 'react'
import { DefaultFooter } from '@ant-design/pro-layout'
import { GithubOutlined } from '@ant-design/icons'
import { Gitee } from '@/common/constants'

const links = [
  {
    key: 'jsfront',
    title: 'JSFRONT',
    href: 'https://gitee.com/jsfront',
    blankTarget: true,
  },
  {
    key: 'github',
    title: <GithubOutlined />,
    href: Gitee,
    blankTarget: true,
  },
  {
    key: 'Ant Design Pro',
    title: 'Ant Design Pro',
    href: 'https://pro.ant.design',
    blankTarget: true,
  },
]

export default () => <DefaultFooter links={links} copyright="2022  前端联盟出品" />
