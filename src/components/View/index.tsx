import React, { FC } from 'react'
import classnames from 'classnames'
import { Card } from 'antd'
import './view.less'

export type IProps = {
  title?: string
  className?: string
  loading?: boolean
  inner?: boolean
  extra?: any
  footer?: any
  footerExtra?: any
  footerTitle?: any
  footerStyle?: React.CSSProperties
  footerClassName?: string
}

const View: FC<IProps> = (props) => {
  const { children, footer, footerStyle, inner = true, footerExtra, footerTitle, className, footerClassName } = props
  const prefixCls = 'app-card-view-footer'
  let foot: React.ReactNode

  if (footerTitle || footer || footerExtra) {
    foot = (
      <div className={`${prefixCls}-wrapper`}>
        {footerTitle && <div className={`${prefixCls}-title`}>{footerTitle}</div>}
        <div className={`${prefixCls}-body`}>{footer}</div>
        {footerExtra && <div className={`${prefixCls}-extra`}>{footerExtra}</div>}
      </div>
    )
  }

  return (
    <Card {...props} className={classnames('app-card-view-container', className)}>
      <div
        className={classnames({
          'app-card-view-inner': inner,
        })}
      >
        {children}
      </div>
      {foot && (
        <div className={classnames(prefixCls, footerClassName)} style={footerStyle}>
          {foot}
        </div>
      )}
    </Card>
  )
}

export default View
