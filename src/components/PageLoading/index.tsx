import React from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'

const antIcon = <LoadingOutlined style={{ fontSize: 35 }} spin />

const PageLoading: React.FC<{ tip?: string }> = ({ tip }) => (
  <div style={{ paddingTop: 100, textAlign: 'center' }}>
    <Spin size="large" indicator={antIcon} tip={tip} />
  </div>
)

// import React, { Fragment, useEffect } from 'react'
// import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'

// function PageLoading() {
//   useEffect(() => {
//     const progress = NProgress.start()
//     return () => {
//       progress.done()
//     }
//   }, [])
//   return <></>
// }

export default PageLoading
