import React from 'react'
import classnames from 'classnames'
import Video from './video'
import './video.less'
import { Col, Row } from 'antd'
import { BackwardOutlined, ForwardOutlined, PlayCircleOutlined } from '@ant-design/icons'

interface Iprops {
  videoList: any
  className?: string
}

const VideoPage: React.FC<Iprops> = ({ videoList, className }) => {
  return (
    <div className={classnames('app-video', className)}>
      <div className="app-video-head">
        <Row gutter={20}>
          {videoList.length > 0 &&
            videoList.map((v: any) => {
              return (
                <Col key={v.url} span={12}>
                  <Video url={v.url} />
                </Col>
              )
            })}
        </Row>
      </div>
      <div className="app-video-progess-wrap">
        <div className="app-video-progress">
          <div className="app-progress-bar" />
        </div>
        <div className="app-progress-time">
          <div className="current-time">0:00</div>
          <div className="current-duration">10:00</div>
        </div>
      </div>
      <div className="app-video-action">
        <div className="back">
          <BackwardOutlined />
        </div>
        <div className="play">
          <PlayCircleOutlined />
        </div>
        <div className="forward">
          <ForwardOutlined />
        </div>
        <div className="times">1x</div>
        <div className="times-list">
          <div className="speed">1x</div>
          <div className="speed">2x</div>
          <div className="speed">4x</div>
          <div className="speed">8x</div>
        </div>
      </div>
    </div>
  )
}
export default VideoPage
