import React from 'react'
import classnames from 'classnames'
import './video.less'

interface Iprops {
  className?: string
  url: string
}

const Video: React.FC<Iprops> = ({ className, url }) => {
  return (
    <div className={classnames('app-video-item', className)}>
      <video className="app-video-main" controls width="100%">
        <source src={url} type="video/mp4" />
      </video>
    </div>
  )
}

export default Video
